import twap_lib

def test_compute_weighted_price():
    prices = []
    weights = []
    assert None == twap_lib.compute_weighted_price(prices, weights)

    prices = [1]
    weights = []
    assert None == twap_lib.compute_weighted_price(prices, weights)
    
    prices = []
    weights = [1]
    assert None == twap_lib.compute_weighted_price(prices, weights)
    
    prices = [1, 2]
    weights = []
    assert None == twap_lib.compute_weighted_price(prices, weights)
    
    prices = [1, 2]
    weights = [1, 0]
    assert None == twap_lib.compute_weighted_price(prices, weights)
    
    prices = [10]
    weights = [387]
    assert 10 == twap_lib.compute_weighted_price(prices, weights)
    
    prices = [1, 2]
    weights = [1, 1]
    assert 1.5 == twap_lib.compute_weighted_price(prices, weights)
    
    prices = [1, 0, 3]
    weights = [1, 1, 2]
    assert 1.75 == twap_lib.compute_weighted_price(prices, weights)


def test_step1_get_interval_boundaries():
    timestamp = 100
    time_window_length = 5
    interval_length = 2
    assert None == twap_lib.step1_get_interval_boundaries(
        timestamp, time_window_length, interval_length)
    
    timestamp = 100
    time_window_length = 10
    interval_length = 2
    assert None == twap_lib.step1_get_interval_boundaries(
        timestamp, time_window_length, interval_length)
    
    timestamp = 100
    time_window_length = 12
    interval_length = 3
    assert ([[94, 96], [97, 99], [100, 102], [103, 105]] ==
            twap_lib.step1_get_interval_boundaries(
                timestamp, time_window_length, interval_length))
    
    timestamp = 99
    time_window_length = 12
    interval_length = 3
    assert ([[93, 95], [96, 98], [99, 101], [102, 104]] ==
            twap_lib.step1_get_interval_boundaries(
                timestamp, time_window_length, interval_length))

            
def test_step2_find_interval_timestamps():
    intervals = [[93, 95]]
    data_timestamps = [90, 100]
    assert [[90]] == twap_lib.step2_find_interval_timestamps(intervals,
                                                             data_timestamps)
    
    intervals = [[93, 95]]
    data_timestamps = [90, 93, 100]
    assert [[93]] == twap_lib.step2_find_interval_timestamps(intervals,
                                                             data_timestamps)
    
    intervals = [[93, 95]]
    data_timestamps = [90, 95, 100]
    assert [[90, 95]] == (
        twap_lib.step2_find_interval_timestamps(intervals, data_timestamps))
    
    intervals = [[93, 95]]
    data_timestamps = [90, 92, 94, 100]
    assert [[92, 94]] == (
        twap_lib.step2_find_interval_timestamps(intervals, data_timestamps))
    
    intervals = [[93, 95]]
    data_timestamps = [90, 92, 93, 94, 95, 96, 100]
    assert [[93, 94, 95]] == (
        twap_lib.step2_find_interval_timestamps(intervals, data_timestamps))

    
def test_step3_get_interval_price_data():
    interval_timestamps = [[90]]
    data_timestamps = [80, 90, 100]
    weighted_prices = [1, 2]
    assert None == twap_lib.step3_get_interval_price_data(
        interval_timestamps, data_timestamps, weighted_prices)
    
    interval_timestamps = [[90]]
    data_timestamps = [80, 90]
    weighted_prices = [1, 2, 3]
    assert None == twap_lib.step3_get_interval_price_data(
        interval_timestamps, data_timestamps, weighted_prices)
    
    interval_timestamps = [[90]]
    data_timestamps = [80, 90, 100]
    weighted_prices = [1, 2, 3]
    assert [[2]] == twap_lib.step3_get_interval_price_data(
        interval_timestamps, data_timestamps, weighted_prices)
    
    interval_timestamps = [[90, 100]]
    data_timestamps = [80, 90, 100]
    weighted_prices = [1, 2, 3]
    assert [[2, 3]] == twap_lib.step3_get_interval_price_data(
        interval_timestamps, data_timestamps, weighted_prices)
    
    interval_timestamps = [[80, 90], [90, 100]]
    data_timestamps = [80, 90, 100]
    weighted_prices = [1, 2, 3]
    assert [[1, 2], [2, 3]] == twap_lib.step3_get_interval_price_data(
        interval_timestamps, data_timestamps, weighted_prices)

    
def test_step4_compute_timeweighted_average_price():
    interval_price_data = []
    assert None == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[]]
    assert None == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[2]]
    assert 2 == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[1], [3]]
    assert 2 == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[1, 3]]
    assert 2 == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[1, 5, 3]]
    assert 2.5 == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[1, 0, 3]]
    assert 1.75 == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
    
    interval_price_data = [[1, 3], [3]]
    assert 2.5 == twap_lib.step4_compute_timeweighted_average_price(
        interval_price_data)
