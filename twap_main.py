import os

from decimal import Decimal

import twap_lib

CONST_data_file = 'raw_price_data.csv'
CONST_twap30_output_file = 'twap30_data.csv'
CONST_twap60_output_file = 'twap60_data.csv'

def convert_input_to_data_point(items):
    """Converts one data file entry into a more convenient dictionary.

    Args:
      items: A list containing the different values from one data file entry.
      
    Returns:
      A dictionary containing the different values using more descriptive names.
    """    
    data_point = {}
    data_point["block"] = int(items[0])
    data_point["timestamp"] = int(items[1])
    data_point["eth_usd_price"] = Decimal(items[2])
    data_point["hex_usd_price"] = Decimal(items[3])
    data_point["hex_usd_liquidity"] = Decimal(items[4])
    data_point["hex_eth_price"] = Decimal(items[5])
    data_point["hex_eth_liquidity"] = Decimal(items[6])
    return data_point


def read_data_file():
    """Reads and parses the entire data file.

    Returns:
      A list of data points extracted from the data file.
    """    
    data = []
    with open(CONST_data_file) as data_file:
        # Ignore the CSV header line.
        header = data_file.readline()
        while True:
            line = data_file.readline()
            if not line:
                break
            items = line.strip().split(',')
            data.append(convert_input_to_data_point(items))
    return data


def generate_twap_dump(output_file_path, time_window_minutes):
    """Computes the time-weighted average price for every timestamp contained
    in the data file and dumps the results into a file.

    Args:
      output_file_path: A path to a file where the time-weighted average prices
                        should be written to.
      time_window_minutes: The length of the time window (in minutes).
    """  
    data = read_data_file()
    data_timestamps = []
    weighted_prices = []
    for i in range(len(data)):
        data_timestamps.append(data[i]["timestamp"])
        prices = [data[i]["hex_usd_price"],
                  data[i]["hex_eth_price"] * data[i]["eth_usd_price"]]
        weights = [data[i]["hex_usd_liquidity"], data[i]["hex_eth_liquidity"]]
        weighted_prices.append(twap_lib.compute_weighted_price(prices, weights))
    with open(output_file_path, "w") as output_file:
        for i in range(len(data)):
            timestamp = data[i]["timestamp"]
            block = data[i]["block"]
            # We need to make sure the data file contains enough data before and
            # after the timestamp, since we need to consider all weighted prices
            # for the entire time window.
            if timestamp < 1626240039:
                continue
            if timestamp > 1628615451:
                continue
            twap, _ = twap_lib.compute_timeweighted_average_price(timestamp,
                data_timestamps, weighted_prices, time_window_minutes)
            output_file.write('{},{},{}\n'.format(timestamp, block, twap))


generate_twap_dump(CONST_twap30_output_file, 30)
generate_twap_dump(CONST_twap60_output_file, 60)
