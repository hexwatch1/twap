from decimal import Decimal
from statistics import mean

def compute_weighted_price(prices, weights):
    """Computes the weighted price of an asset by considering each price point
    with the given corresponding weight.

    Args:
      prices: The various prices of the given asset.
      weights: The corresponding weights that each price point should be given.

    Returns:
      The weighted price.
    """
    if len(prices) != len(weights) or not len(weights):
        return None
    result = Decimal(0)
    for i in range(len(prices)):
        if weights[i] <= 0:
            return None
        result += prices[i] * weights[i]
    return result / sum(weights)
   

def step1_get_interval_boundaries(timestamp, time_window_length,
                                  interval_length):
    """Computes the time interval boundaries needed to compute the time-weighted
    average price with the specified time window and interval length.

    The returned list will contain time_window_length / interval_length
    time intervals. The specified timestamp represents the midpoint of the
    entire time window. For example, if time window length = 30 minutes, then 15
    minutes come before and 15 minutes come after the provided timestamp.

    Args:
      timestamp: The timestamp for which to calculate the time-weighted average
                 price.
      time_window_length: The length of the time window (in seconds).
      interval_length: The length of the intervals to divide the time window
                       into (in seconds).

    Returns:
      The interval boundaries needed to compute the time-weighted average price.
    """
    if time_window_length % interval_length != 0:
        return None
    num_intervals = int(time_window_length / interval_length)
    # We need an even # of intervals otherwise we can't make timestamp be
    # exactly in the middle.
    if num_intervals % 2 != 0:
        return None
    current_timestamp = int(timestamp - interval_length * num_intervals / 2)
    intervals = []
    for i in range(num_intervals):
        intervals.append(
            [current_timestamp, current_timestamp + interval_length - 1])
        current_timestamp += interval_length
    return intervals
    

def step2_find_interval_timestamps(intervals, data_timestamps):
    """Modifies the time intervals so that they now contain all relevant block
    times rather than start and end times.

    The previous interval start and end times are replaced by the block time of
    the blocks valid at those points in time. If additional blocks were created
    in-between, they are also added to the list. For example, [1:10:35, 1:10:39]
    may become [1:10:31, 1:10:37, 1:10:39].

    Args:
      intervals: The previously computed time interval boundaries.
      data_timestamps: The timestamps of each committed block.

    Returns:
      The relevant timestamps for each time interval needed to compute the
      time-weighted average price.
    """
    # Optimization: Find the first interval start time in the timestamps list so
    # that we can avoid repeatedly iterating over smaller timestamps.
    start_index = 0
    while data_timestamps[start_index] <= intervals[0][0]:
        start_index += 1
    start_index = max(0, start_index - 2)
    interval_timestamps = []
    for i in range(len(intervals)):
        interval_start = intervals[i][0]
        interval_end = intervals[i][1]
        j = start_index
        # After this loop, j will be the timestamp after the one we are
        # interested in.
        while data_timestamps[j] <= interval_start:
            j += 1
        current_interval = [data_timestamps[j - 1]]
        # Add all timestamps up to and including the interval end time. We do
        # not want to ignore blocks which were added during the interval.
        while data_timestamps[j] <= interval_end:
            current_interval.append(data_timestamps[j])
            j += 1
        interval_timestamps.append(current_interval)
    return interval_timestamps


def step3_get_interval_price_data(interval_timestamps, data_timestamps,
                                  weighted_prices):
    """Replaces the timestamps for each time interval with their weighted asset
    prices valid at those times.

    Args:
      interval_timestamps: The relevant timestamps for each time interval.
      data_timestamps: The timestamps of each committed block.
      weighted_prices: The weighted prices valid at each committed block.

    Returns:
      A list of weighted prices for each time interval needed to compute the
      time-weighted average price.
    """
    if len(data_timestamps) != len(weighted_prices):
        return None
    index = 0
    interval_price_data = []
    for i in range(len(interval_timestamps)):
        current_interval = []
        for j in range(len(interval_timestamps[i])):
            # We assume that the timestamp we are looking for is included in
            # the data_timestamps list.
            while data_timestamps[index] < interval_timestamps[i][j]:
                index += 1
            current_interval.append(weighted_prices[index])
        interval_price_data.append(current_interval)
    return interval_price_data


def step4_compute_timeweighted_average_price(interval_price_data):
    """Computes the time-weighted average price of an asset.

    For each time interval we compute the average of Open, High, Low, Close.
    The time-weighted average price is the average of all time intervals.

    Args:
      interval_price_data: A list of weighted prices for each time interval.
      
    Returns:
      The time-weighted average price of the asset.
    """
    intervals = []
    if len(interval_price_data) <= 0:
        return None
    for i in range(len(interval_price_data)):
        if len(interval_price_data[i]) <= 0:
            return None
        # In case of a time interval only having one relevant weighted price,
        # use this price directly rather than risking potential rounding errors.
        elif len(interval_price_data[i]) == 1:
            intervals.append(interval_price_data[i][0])
        else:
            price_open = interval_price_data[i][0]
            price_high = max(interval_price_data[i])
            price_low = min(interval_price_data[i])
            price_close = interval_price_data[i][-1]
            intervals.append(
                mean([price_open, price_high, price_low, price_close]))
    return mean(intervals)


def compute_timeweighted_average_price(timestamp, data_timestamps,
                                       weighted_prices, time_window_minutes):
    """Computes the time-weighted average price of an asset.

    This function should be used rather than calling the various steps
    individually.
    
    Args:
      timestamp: The timestamp for which to calculate the time-weighted average
                 price.
      data_timestamps: The timestamps of each committed block.
      weighted_prices: The weighted prices valid at each committed block.
      time_window_minutes: The length of the time window (in minutes).
      
    Returns:
      The time-weighted average price of the asset.
    """
    if len(data_timestamps) != len(weighted_prices):
        return None
    interval_boundaries = step1_get_interval_boundaries(
        timestamp,time_window_minutes * 60, 5)
    interval_timestamps = step2_find_interval_timestamps(
        interval_boundaries, data_timestamps)
    interval_price_data = step3_get_interval_price_data(
        interval_timestamps, data_timestamps, weighted_prices)
    twap = step4_compute_timeweighted_average_price(interval_price_data)
    return twap, interval_timestamps
