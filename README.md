# HEX Time-weighted Average Price Readme

* This code can be used to calculate the time-weighted average price of HEX. The data file contains all required
data between July 14 2021 05:20:39 UTC and August 10 2021 17:10:51 UTC.

* All data is queried per-block. The Ethereum block times come from web3.eth API, get_block()'s timestamp. The Uniswap V2 data comes from The Graph's uniswap/uniswap-v2 subgraph.

* On a modern computer it should take less than 1 hour to compute all time-weighted average prices for a specific time interval size. As such, not much effort was put into optimizing the code further.

* The algorithm works as follows:

  * 1. Specify the timestamp at which the time-weighted average price should be computed as well as the size of the time window. The entire time window is then split into 5-second time intervals.

  * 2. For each time interval find the relevant ETH block times. The interval start and end time are replaced by their previous block time (since the price valid at a specific time point depends on the data of the latest committed block). Should additional blocks have been committed during an interval, they are added as well.

  * 3. For each time interval look up the corresponding weighted prices at each of the relevant ETH block times.

  * 4. For each time interval calculate the average of Open, High, Low, Close. The time-weighted average price is then computed by taking the average over all time intervals.
